"""
Django settings for Papyrus project.
"""

import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


SECRET_KEY = 'kk7my=1ob1r(9bwx=#&kj6encs!29j5_32i6v2wuy@b$cgz=^y'

DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

INSTALLED_APPS = (
    'suit',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'markitup',
    'south',
    'blog',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.core.context_processors.csrf',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.request',
    'papyrus.papyrus_context_processors.main',
)

ROOT_URLCONF = 'papyrus.urls'

WSGI_APPLICATION = 'papyrus.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite'),
    }
}

LANGUAGE_CODE = 'en-us'

# TIME_ZONE = 'UTC'
TIME_ZONE = 'Europe/Kiev'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'papyrus/public/static/')
STATICFILES_DIRS = (
    # os.path.join(BASE_DIR, 'papyrus/public/static/'),
    os.path.join(BASE_DIR, 'papyrus/static/'),
)


MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'papyrus/public/media/')

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'papyrus/templates'),
)

MARKITUP_FILTER = ('markdown.markdown', {'safe_mode': True})
