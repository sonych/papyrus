# coding: utf-8


def main(request):
    """ Main Papyrus context processor.
        Add into context basic vars of project.
    """

    # TODO: вынести все эти переменные в настройки проекта

    return {
        'PAPYRUS_TITLE': 'Sonych KO',
        'PAPYRUS_AUTHOR': 'Sonych Mykolai',
        'PAPYRUS_DESCRIPTION': 'Some good description',
        'PAPYRUS_KEYWORDS': 'python, linux, django',
        'PAPYRUS_LOGO_TEXT': 'Sonych KO',
        'PAPYRUS_LOGO_DESC': 'fucking blog',
        'PAPYRUS_FOOTER_TEXT': 'Sonych',
        'PAPYRUS_FOOTER_YEARS': '2013',
    }
