# coding: utf-8

from django.conf.urls import patterns, include, url
from django.conf import settings

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^odmin/', include(admin.site.urls)),
    url(r'^markitup/', include('markitup.urls')),

    url(r'^blog/', include('blog.urls')),
    url(r'^$', 'blog.views.index', name='blog_index'),
)


if getattr(settings, 'DEBUG' , False):
    urlpatterns += patterns('django.contrib.staticfiles.views',
        url(r'^static/(?P<path>.*)$', 'serve'),
    )
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': getattr(settings, 'MEDIA_ROOT' , ''),
            'show_indexes': True,
        }),
    )
