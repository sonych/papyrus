# coding: utf-8

from django.shortcuts import render
from django.http import Http404

from .models import Post


def index(request):
    template_name = 'blog/index.html'

    return render(request, template_name, {
            'posts': Post.objects.filter(active=True).order_by('pub_date')
        })


def post(request, post_slug):
    template_name = 'blog/blog_post.html'

    try:
        post = Post.objects.get(slug=post_slug, active=True)
    except Post.DoesNotExist:
        raise Http404

    return render(request, template_name, {
            'post': post,
        })
