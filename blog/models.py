# coding: utf-8

from django.db import models
from django.utils.translation import ugettext_lazy as _

from markitup.fields import MarkupField


class Tag(models.Model):
    name = models.CharField(_(u'Name'), max_length=50, unique=True)

    def __unicode__(self):
        return self.name


class Post(models.Model):
    title = models.CharField(_(u'Title'), max_length=100)
    slug = models.SlugField(max_length=100, unique=True)
    active = models.BooleanField(_(u'Active'), default=False)
    pub_date = models.DateTimeField(_(u'Pub date'), auto_now_add=True)
    update_date = models.DateTimeField(_(u'Update date'), auto_now=True)
    short_text = models.TextField(_(u'Short text'), blank=True)
    text = MarkupField(_(u'Text'))
    tags = models.ManyToManyField(Tag, verbose_name=_('Tags'), blank=True, null=True)

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('blog_post', args=[self.slug,])

    def get_tags(self):
        return self.tags.all()
