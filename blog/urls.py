# coding: utf-8

from django.conf.urls import *

urlpatterns = patterns('blog.views',
    url(r'^(?P<post_slug>[-\w]+)/$', 'post', name='blog_post'),
)

