# coding: utf-8

from django.contrib import admin

from .models import Tag, Post


class PostAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}


admin.site.register(Tag)
admin.site.register(Post, PostAdmin)
